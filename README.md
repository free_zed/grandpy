GrandPy Bot
===========

A _robot_ grandfather talking about geographic memories and quoting Wikipedia.

If you can speak French, [have a talk with him][prod] (^_^)

---

What ?
------

This was a [student project][oc] previously hosted on [Github][ocp7]. I updated it to play with :

* [Zappa][zappa]
* [Flask][flask]
* [AWS Lambda][awslmbd]
* [Gitlab-ci][glci]

How ?
-----

Need some environment variables to run :

- `EMAIL_ADDRESS` (Nominatim API)
- `MAPBOX_API_KEY` (Mapbox API)
- `GOO_API_KEY` (not used [but needed][issue51])


### Local run

It works as a simple single-page [Flask][flask] application :

- [fork-it][fork]
- set up a virtualenv : `virtualenv .venv`
- install requirements :`pip install -r requirements.txt`
- run it locally : `python run.py`

### Gitlab run

Set up a [AWS IAM profile][awsiam] on **your AWS account** and add valid environment variables in `https://gitlab.com/<namespace>/grandpy/settings/ci_cd` :

- `AWS_ACCESS_KEY_ID`
- `AWS_SECRET_ACCESS_KEY`

Push it back and the _CI script_ will :

- put chat code on [AWS Lambda][awslmbd] with the magic of [Zappa][zappa]
- edit [JavaScript][js] with the URL of the _AWS API Gateway_ just updated
- generate the HTML landing page (with a [flask script][genstat])
- deploy statics on _gitlab-pages_ (HTML, images & CSS)

…and _voilà_, have a chat with _GrandPy_ on `https://<namespace>.gitlab.io/grandpy/` (-;

---

Feel free to give feedback using [issues][issues].

[issue51]: https://gitlab.com/free_zed/grandpy/issues/
[awsenv]: https://gitlab.com/free_zed/grandpy/blob/master/aws-auth.sh
[awsiam]: https://github.com/Miserlou/Zappa/blob/master/example/policy/deploy.json
[awslmbd]: https://console.aws.amazon.com/lambda/
[flask]: http://flask.pocoo.org/
[fork]: https://gitlab.com/free_zed/grandpy/forks/new
[genstat]: https://gitlab.com/free_zed/grandpy/blob/master/genstat.py
[glci]: https://gitlab.com/free_zed/grandpy/blob/master/.gitlab-ci.yml
[issues]: https://gitlab.com/free_zed/grandpy/issues/new
[js]: https://gitlab.com/free_zed/grandpy/tree/master/static/js/ask.js
[oc]: https://openclassrooms.com/fr/projects/creez-grandpy-bot-le-papy-robot
[ocp7]: https://github.com/freezed/ocp7
[prod]: https://free_zed.gitlab.io/grandpy
[zappa]: https://github.com/Miserlou/Zappa/
