#!/usr/bin/env python3
# coding: utf8

"""
Author: [freezed](https://gitlab.com/free_zed) 2018-08-25
Licence: `GNU GPL v3` GNU GPL v3: http://www.gnu.org/licenses/

This file is part of project [grandpy](https://gitlab.com/free_zed/grandpy/).
"""
from random import choice as rc
from config import MAP_LINK


class Message:
    """ Class doc """

    def __init__(self, place):
        """ Class initialiser """
        self.place = place
        self.babble = {
            "address_yes": [
                "Bien sûr mon poussin! Voici l'adresse",
                "Mais oui mon p'tit, tiens mon neurone vient de démarrer, voici l'adresse",
                "Oh oui, je m'en souviens bien, et hop, l'adresse",
                "Ouh, ça fait un bail! Mais si ma mémoire est bonne l'adresse est celle ci",
            ],
            "extract_yes": [
                "D'ailleurs j'ai passé un bout de temps dans le coin, laisse moi t'en causer un peu",
                "GrandMy (ma femme) adorait cet endroit, on y est allé souvent",
                "Là aussi on a passé de bons moments… Attends un peu que je te raconte",
                "J'y ai fais les 400 coups bien avant que tu mettes ta première couche-culotte",
            ],
            "address_no": [
                "Ça me dit rien gamin, reformule pour voir…",
                "Moi je gâtouille pas encore, par contre toi c'est moins sûr… Repose ta question!",
                "C'est pas très clair comme question, répète un peu…",
                "Nan mais j'suis pas prix Nobel moi!!",
            ],
            "extract_no": [
                "Euh, faut que je prenne mes pilules…",
                "Je t'ai dis que c'était l'heure de la sieste…",
                "Nan, mais j'ai déjà raconté cette histoire 100 fois!",
                "Et sinon, t'as pas des amis à qui causer?",
            ],
        }

    def address_no(self):
        return {"address": rc(self.babble["address_no"]), "map_link": False}

    def extract_no(self):
        return {"extract": rc(self.babble["extract_no"]), "curid": False}

    def address_yes(self):
        return {
            "map_img_src": self.place.get_map_src(),
            "map_link": MAP_LINK.format(
                lat=self.place.geo_data["lat"], lon=self.place.geo_data["lon"]
            ),
            "address": "{} : {}".format(
                rc(self.babble["address_yes"]), self.place.geo_data["display_name"]
            ),
        }

    def extract_yes(self):
        return {
            "curid": self.place.article_data["pageid"],
            "extract": "{} : {}………".format(
                rc(self.babble["extract_yes"]), self.place.article_data["extract"][:200]
            ),
        }
